package numeroingresado1;

import javax.swing.JOptionPane;

public class NumeroIngresado1 {

    public static void main(String[] args) {
        JOptionPane.showMessageDialog(null, "Bienvenido", "Numero Ingresado", JOptionPane.INFORMATION_MESSAGE);

        double numeros[] = new double[6];
        JOptionPane.showMessageDialog(null, "Ingrese 6 números >= 15.12 y <= 19.31", "Numero Ingresado", JOptionPane.INFORMATION_MESSAGE);

        for (double i = 0; i < numeros.length; i++) {
            double numero = Double.parseDouble(JOptionPane.showInputDialog(null, "Inserte numero"));

            if (numero <= 19.31 == numero >= 15.12) {
                double resultado = (numero * 100 / 2) + 100;
                JOptionPane.showMessageDialog(null, "Respuesta: " + resultado, "Numero Ingresado", JOptionPane.INFORMATION_MESSAGE);

            } else {
                JOptionPane.showMessageDialog(null, "ERROR: El número no está dentro del rango", "Numero Ingresado", JOptionPane.ERROR_MESSAGE);
            }
        }

    }
}
