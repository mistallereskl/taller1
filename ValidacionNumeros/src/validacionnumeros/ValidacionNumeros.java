package validacionnumeros;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class ValidacionNumeros {

    public static void main(String[] args) {
        JOptionPane.showMessageDialog(null, "Bienvenido", "Validación números", JOptionPane.INFORMATION_MESSAGE);

        int numeros[] = new int[5];
        Scanner sc = new Scanner(System.in);
        JOptionPane.showMessageDialog(null, "Ingrese 5 números", "Validación números", JOptionPane.INFORMATION_MESSAGE);

        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = Integer.parseInt(JOptionPane.showInputDialog(null, "Inserte numero"));
        }

        for (int i = 0; i < numeros.length; i++) {
            if (74 <= numeros[i] && numeros[i] <= 189) {
                if (numeros[i] % 2 == 0) {
                    if (numeros[i] % 5 == 0) {
                        JOptionPane.showMessageDialog(null, "Número Valido: " + numeros[i], "Validación números", JOptionPane.INFORMATION_MESSAGE);

                    }
                }
            }//cierra IF intervalo
        }//cierra for

    }//cierra main

}//cierra class
