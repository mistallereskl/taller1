package factorial;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class Factorial {

    public static void main(String[] args) {
        JOptionPane.showMessageDialog(null, "Bienvenido", "Factorial", JOptionPane.INFORMATION_MESSAGE);
        Scanner sc = new Scanner(System.in);
        try {
            int factorial;
            int numero = 10;
            factorial = 1;
            for (int i = numero; i > 0; i--) {
                factorial = factorial * i;
            }
            if (factorial == 3628800) {
                JOptionPane.showMessageDialog(null, "El resultado es: " + factorial, "Resultado", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "La operación no se pudo realizar ", "Factorial", JOptionPane.ERROR_MESSAGE);
            }

        }//Cierra try
        catch (Exception e) {
            System.out.println("ERROR: " + e);
        }//Cierra catch        
    }//Cierra main
}//Cierra class
