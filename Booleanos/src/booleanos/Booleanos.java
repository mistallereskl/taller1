package booleanos;

import java.util.Scanner;

public class Booleanos {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Ingrese dos datos booleanos:");
        boolean booleanUno = Boolean.parseBoolean(sc.nextLine());
        boolean booleanDos = Boolean.parseBoolean(sc.nextLine());

        System.out.println("¿Cuantas veces desa imprimir el mensaje?");
        int veces = Integer.parseInt(sc.nextLine());

        System.out.println("Las operaciones realizables son:");
        System.out.println("1. OR");
        System.out.println("2. AND");
        System.out.println("3. XOR");
        System.out.println("Ingrese la opción deseada:");

        int opcion = Integer.parseInt(sc.nextLine());

        if (0 < opcion && opcion < 4) {

            switch (opcion) {
                case 1:
                    if (booleanUno || booleanDos == true) {
                        int contador = 0;
                        for (int i = 0; i < veces; i++) {
                            System.out.println("True");
                            contador += 1;
                        }
                    } else {
                        int contador = 0;
                        for (int i = 0; i < veces; i++) {
                            System.out.println("False");
                            contador += 1;
                        }
                    }
                    break;
                case 2:
                    if (booleanUno && booleanDos == true) {
                        int contador = 0;
                        for (int i = 0; i < veces; i++) {
                            System.out.println("True");
                            contador += 1;
                        }
                    } else {
                        int contador = 0;
                        for (int i = 0; i < veces; i++) {
                            System.out.println("False");
                            contador += 1;
                        }
                    }
                    break;
                case 3:
                    if (booleanUno ^ booleanDos == true) {
                        int contador = 0;
                        for (int i = 0; i < veces; i++) {
                            System.out.println("True");
                            contador += 1;
                        }//cierra FOR
                    } else {
                        int contador = 0;
                        for (int i = 0; i < veces; i++) {
                            System.out.println("False");
                            contador += 1;
                        }
                    }
                    break;

            }//cierra switch

        }else{System.out.println("Opción no valida.");}

    }//cierra main
}// cierra class