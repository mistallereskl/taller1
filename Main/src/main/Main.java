package main;

import javax.swing.JOptionPane;

public class Main {

    public static void main(String[] args) {
        double resultado = 0;
        JOptionPane.showMessageDialog(null, "Bienvenido", "Mi calculadora", JOptionPane.INFORMATION_MESSAGE);

        try {
            double numero = Double.parseDouble(JOptionPane.showInputDialog(null, "Inserte primer numero"));
            double numero2 = Double.parseDouble(JOptionPane.showInputDialog(null, "Inserte Segundo numero"));
            double numero3 = Double.parseDouble(JOptionPane.showInputDialog(null, "Inserte tercer numero"));
            String operador = new String();
            operador = JOptionPane.showInputDialog(null, "Inserte operador");
            System.out.println("OPERADOR: " + operador);
            if (operador.equals("+")) {
                resultado = numero + numero2 + numero3;
                System.out.println("estoy en la suma");
            } else if (operador.equals("-")) {
                resultado = numero - numero2 - numero3;
                System.out.println("estoy en la resta");
            }
            else if (operador.equals("*")) {
                resultado = numero * numero2 * numero3;
                System.out.println("estoy en la multiplicación");
            } else if (operador.equals("%")){
                resultado = numero % numero2 % numero3;
                System.out.println("estoy en el modulo");                
            } else if (operador.equals("/")){
                resultado = numero / numero2 / numero3;
                System.out.println("estoy en la división");                
            }
            
            else {
                JOptionPane.showMessageDialog(null, "Solo sumo, resto, multiplico, divido y modulo. ", "Resultado", JOptionPane.ERROR_MESSAGE);
            }
            JOptionPane.showMessageDialog(null, "Resultado: " + resultado, "Resultado", JOptionPane.INFORMATION_MESSAGE);

        } catch (Exception e) {
            System.out.println("ERROR: " + e);
        } finally {
        }
    }
}
