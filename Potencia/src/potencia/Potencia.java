package potencia;

import java.util.Scanner;
import javax.swing.JOptionPane;

public class Potencia {

    public static void main(String[] args) {
        JOptionPane.showMessageDialog(null, "Bienvenido", "Potencia", JOptionPane.INFORMATION_MESSAGE);
        Scanner sc = new Scanner(System.in);

        JOptionPane.showMessageDialog(null, "Recuerde que la cantidad de números que debe ingresar debe ser mayor o igual a 10 (el primer digito debe ser distinto de cero)", "Potencia", JOptionPane.INFORMATION_MESSAGE);

            double numero = Double.parseDouble(JOptionPane.showInputDialog(null, "Inserte primer numero"));

            if (numero >= 1000000000) {
                double cuarta = Math.pow(numero, 4);

                JOptionPane.showMessageDialog(null, "La potencia es: " + cuarta, "Potencia", JOptionPane.INFORMATION_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, "ERROR: El número que debía ingresar es menor a 10 dígitos, por favor intente nuevamente", "Poencia", JOptionPane.ERROR_MESSAGE);

            }
            double numero2 = Double.parseDouble(JOptionPane.showInputDialog(null, "Inserte segundo numero"));

            if (numero2 >= 1000000000) {
                double cuarta2 = Math.pow(numero2, 4);

                JOptionPane.showMessageDialog(null, "La potencia es: " + cuarta2, "Potencia", JOptionPane.INFORMATION_MESSAGE);

            } else {
                JOptionPane.showMessageDialog(null, "ERROR: El número que debía ingresar es menor a 10 dígitos", "Potencia", JOptionPane.ERROR_MESSAGE);

            }
            double numero3 = Double.parseDouble(JOptionPane.showInputDialog(null, "Inserte tercer numero"));

            if (numero3 >= 1000000000) {
                double cuarta3 = Math.pow(numero3, 4);

                JOptionPane.showMessageDialog(null, "La potencia es: " + cuarta3, "Potencia", JOptionPane.INFORMATION_MESSAGE);

            } else {
                JOptionPane.showMessageDialog(null, "ERROR: El número que debía ingresar es menor a 10 dígitos", "Potencia", JOptionPane.ERROR_MESSAGE);
            }
            double numero4 = Double.parseDouble(JOptionPane.showInputDialog(null, "Inserte cuarto numero"));

            if (numero4 >= 1000000000) {
                double cuarta4 = Math.pow(numero4, 4);

                JOptionPane.showMessageDialog(null, "La potencia es: " + cuarta4, "Potencia", JOptionPane.INFORMATION_MESSAGE);

            } else {
                JOptionPane.showMessageDialog(null, "ERROR: El número que debía ingresar es menor a 10 dígitos", "Potencia", JOptionPane.ERROR_MESSAGE);
            }

    }//Cierra main

}//Cierra class
